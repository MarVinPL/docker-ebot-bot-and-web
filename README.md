docker-eBot-and-WEB
==========

This is a Docker image of eBot.
Please be aware that this is only the server.


example for docker run:
```
docker run -v /root/ebot/apache2:/etc/apache2 -v /root/ebot/bot/demos:/ebot/bot/demos -v /root/ebot/bot/logs:/ebot/bot/logs -v /root/ebot/bot/config:/ebot/bot/config -v /root/ebot/web/config:/ebot/web/config -p 80:81 --name eBot marvin1994pl/docker-ebot-bot-and-web
```


Usage with HTTPS webinterface
-----------------------------

Visit [https://github.com/carazzim0/nginx-eBot](https://github.com/carazzim0/nginx-eBot) for nginx https configuration.
